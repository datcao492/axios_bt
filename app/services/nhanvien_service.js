const BASE_URL = "https://6271e1a6c455a64564b8f20f.mockapi.io";

const nhanVienServ = {
  xuatDanhSach: function () {
    return axios({
      url: `${BASE_URL}/nhan-vien`,
      method: "GET",
    });
  },
};