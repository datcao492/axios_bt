const turnOnLoading = function () {
    document.getElementById("loading").style.display = "flex";
  };
  const turnOffLoading = function () {
    document.getElementById("loading").style.display = "none";
  };

  const renderDanhSachNhanVien = function () {
    turnOnLoading();
  
    nhanVienService
      .xuatDanhSach()
      .then((res) => {
        turnOffLoading();
  
        xuatDanhSachNhanVien(res.data);
      })
      .catch((err) => {
        turnOffLoading();
      });
  };
  renderDanhSachNhanVien();

  const xoaNhanVienService = function (id) {
    turnOnLoading();
    nhanVienService
      .xoaDanhSach(id)
      .then((res) => {
        turnOffLoading();
  
        renderDanhSachNhanVien();
      })
      .catch((err) => {
        turnOffLoading();
      });
  };


  const xuatDanhSachNhanVien = function (dsnd) {
    var contentHTML = "";
  
    dsnd.forEach(function (item){
      switch (item.language) {
        case "Monday":
          item.language = "FRENCH";
          break;
        case "Tuesday":
          item.language = "JAPANESE";
          break;
        case "Wednesday":
          item.language = "CHINESE";
          break;
        case "Thursday":
          item.language = "RUSSIAN";
          break;
        case "Friday":
          item.language = "SWEDEN";
          break;
        case "Saturday":
          item.language = "SPANISH";
          break;
        case "Sunday":
          item.language = "ITALIAN";
          break;
      }
      var contentTr = `<tr>
      <td>${item.id}</td>
      <td>${item.taikhoan}</td>
      <td>${item.matkhau}</td>
      <td>${item.hoten}</td>
      <td>${item.email}</td>
      <td>${item.ngonngu}</td>
      <td>${item.loai ? "Giáo Viên" : "Học Viên"}</td>
      <td>${item.mota}</td>
      <td>
      <button data-toggle="modal"
      data-target="#myModal" onclick="layThongTinNhanVien(${
        item.id
      })" class="btn btn-success my-2">Sửa</button>
      <button onclick="xoaNhanVienService(${
        item.id
      })" class="btn btn-danger">Xóa</button>
      </td>
      </tr>`;
  
      contentHTML += contentTr;
    });
    document.getElementById("tblDanhSachNhanVien").innerHTML = contentHTML;
  };

const layThongTinTuForm = function () {
  var TK = document.getElementById("TaiKhoan").value;
  var hoTen = document.getElementById("HoTen").value;
  var matKhau = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var hinhAnh = document.getElementById("HinhAnh").value;
  var loai = document.getElementById("loaiNhanVien").value * 1;
  var ngonNgu = document.getElementById("loaiNgonNgu").value;
  var moTa = document.getElementById("MoTa").value;
  return new NhanVien(
    hoTen,
    matKhau,
    email,
    loai == 1 ? true : false,
    ngonNgu,
    moTa,
    hinhAnh,
    TK
  );
};

